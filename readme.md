

Step 1:

Upload all the contents of this folder to the public_html folder of the website

Step 2:

On the Footer area of the HTML:

Find the line:

```
</body>
```

**Add above and Replace** with

```
<script src="//cdn.jsdelivr.net/npm/jquery-validation@1.19.0/dist/jquery.validate.min.js"></script>
<script src="submission.js"></script>
</body>
```

Step 3:

Find the **'parent div'** with the form such as:

```
<div class="w-form form-wrapper">
 <form id="email-form" name="email-form" data-name="Email Form">
```

Make sure the **action** is set to **'submission.php'** with **method="POST"** and change the form parent class from **w-form** to **w-form-mod** and add class **class="form"** to the form as per: 
```
<div class="w-form-mod form-wrapper">
 <form action="submission.php" method="POST" class="form" id="email-form" name="email-form" data-name="Email Form">
```

Step 4:

Make sure the validation section changes from: 

```
<div class="w-form-done">
 <p>Thank you! Your submission has been received!</p>
</div>
<div class="w-form-fail">
 <p>Oops! Something went wrong while submitting the form</p>
</div>
```

To something like:

```
<div class="w-form-done" style="display: none;">
 <p>Thank you! Your submission has been received!</p>
</div>
<div class="w-form-fail" style="display: none;">
 <p>Oops! Something went wrong while submitting the form</p>
</div>
```

The main important feature is the class name contains '**w-form-done** and **w-form-fail**' and the style is set to '**display:none**'

Notes:


It is important to realise the following fields are submitted (without modification) please note these **must match the 'name' property of the form input fields**. This is setup for you to add to if you wish.

eg:

```
 <input type="text" name="firstName" placeholder="First Name" ...
```

email

firstName

lastName

name

phone

message

To add additional fields, simply open up the submission.php file and copy an existing variable such as:

```
$email = isset($_POST['email']) ? $_POST['email'] : '';
```

and in the htmlMessage variable;
```
<p>Email Address: <strong>$email</strong>";
```

For example, lets use 'Pets Name' it would loo something like:

```
$email = isset($_POST['email']) ? $_POST['email'] : '';
$petsName = isset($_POST['petsName']) ? $_POST['petsName'] : '';
```
And in the htmlMessage..
```
<p>Email Address: <strong>$email</strong>";
<p>Pets Name: <strong>$petsName</strong>";
```

Assuming 'petsName' was the name property of the input field ala
```
 <input type="text" name="petsName" placeholder="Pets Name" ...
```
