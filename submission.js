$(".w-form-mod").each(function () {
    var $thisOwner = $(this);
    var $thisForm = $(this).find('form');
    $thisForm.validate({
        submitHandler: function (form) {
            var myForm = $(form);
            var alert = $thisOwner.find('.w-form-done');
            var submitBtn = myForm.find('.w-button');
            $.ajax({
                url: form.action,
                type: form.method,
                data: $(form).serialize(),
                success: function (response) {
                    alert.show();
                    $(submitBtn).attr("disabled", "disabled");
                    $(myForm).hide();
                }
            });
        }
    });
});